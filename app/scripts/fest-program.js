// Consts
const festMenuItem = document.querySelectorAll('.fest-program__menu-item')
const festSite = document.querySelectorAll('.fest-site__name')
const days = document.querySelectorAll('.fest-program__days-item')
const eventContainer = document.querySelectorAll('.fest-event__container')

for (let i = 0; i < festSite.length; i++) {
  festSite[i].addEventListener('click', function () {
    this.classList.toggle('active')
    let panel = this.nextElementSibling
    if (panel !== null) {
      panel.classList.toggle('active')
      if (panel.style.maxHeight !== '0px') {
        panel.style.maxHeight = '0px'
      } else {
        panel.style.maxHeight = panel.scrollHeight + 'px'
      }
    }

  })
}

function handler() {
  let b = document.querySelectorAll('[data-type]')
  for (let j = 0; j < b.length; j++) {
    b[j].dataset.type.includes(this.dataset.type) ? b[j].classList.add('active') : b[j].classList.remove('active')
  }
}

function setVenueHidden() {
  for (let a = 0; a < eventContainer.length; a++) {
    let counter = 0
    for (let b = 0; b < eventContainer[a].children.length; b++) {
      if(eventContainer[a].children[b].classList.contains('hide')) {
        counter++
      }
    }
    if (eventContainer[a].children.length === counter) {
      eventContainer[a].parentElement.style.display = 'none'
    } else {
      eventContainer[a].parentElement.style.display = 'block'
    }
  }
}

function daysHide() {
  let c = document.querySelectorAll('[data-day]')
  for (let k = 0; k < c.length; k++) {
    !c[k].dataset.day.includes(this.dataset.day) ? c[k].classList.add('hide') : c[k].classList.remove('hide')
    this.dataset.day === 'all' ? c[k].classList.remove('hide') : null
  }
  setVenueHidden()
}

$(function () {
  $('.schedule__grid-mobile-select').change(function () {
    $('.schedule__grid-mobile-item').hide()
    $('#' + $(this).val()).show()
  })
})

for (let i = 0; i < festMenuItem.length; i++) {
  festMenuItem[i].addEventListener('click', handler)
}
for (let i = 0; i < days.length; i++) {
  days[i].addEventListener('click', daysHide)
}
