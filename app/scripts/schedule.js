// Consts
const scheduleItem = document.querySelectorAll('.schedule__grid-item')
const scheduleMenuItem = document.querySelectorAll('.schedule__menu-item')
const topArray = []

// Window events
window.onscroll = () => {
  toggleStickyMenu()
  toggleMenuInfo()
}

window.onresize = () => {
  setFullWidth()
}

function getTopOffset(elem) {
  let box = elem.getBoundingClientRect()
  return box.top + pageYOffset
}

function getMaxOfArray(numArray) {
  return Math.max.apply(null, numArray)
}

function handler() {
  let b = document.querySelectorAll('[data-type]')
  for (let j = 0; j < b.length; j++) {
    b[j].dataset.type.includes(this.dataset.type) ? b[j].classList.add('active') : b[j].classList.remove('active')
  }
}

function toggleStickyMenu() {
  const scheduleMenuSticky = document.querySelector('.schedule__menu--sticky')
  const scheduleMenu = document.querySelector('.schedule__menu')
  const menuTopOffset = getTopOffset(scheduleMenu)
  if (window.pageYOffset >= menuTopOffset * 1.2) {
    scheduleMenuSticky.classList.add('show')
  } else {
    scheduleMenuSticky.classList.remove('show')
  }
}

function setFullWidth() {
  const scheduleFullWidth = document.querySelectorAll('[data-type=\'full\']')
  const scheduleContent = document.querySelector('.schedule__content')
  for (let i = 0; i < scheduleFullWidth.length; i++) {
    scheduleFullWidth[i] !== null ? scheduleFullWidth[i].style.width = `${scheduleContent.offsetWidth - (9 * 10)}px` : null
  }

}

function calcEventHeight() {
  const baseHeight = 160
  for (let i = 0; i < scheduleItem.length; i++) {
    let divPos = scheduleItem[i].children[0].innerText.search(/ – | - /)
    let start = scheduleItem[i].children[0].innerText.substr(0, divPos)
    let end = scheduleItem[i].children[0].innerText.substr(divPos + 2)
    let a = moment(start, 'HH-mm')
    let b = moment(end, 'HH-mm')
    let c = moment('10:00', 'HH-mm')
    scheduleItem[i].style.height = `${(b.diff(a, 'hours', 'minutes')).toFixed(5) * baseHeight}px`
    scheduleItem[i].style.top = `${(a.diff(c, 'hours', 'minutes').toFixed(5) * baseHeight)}px`
    topArray.push(parseInt(scheduleItem[i].style.top))
  }
}

function calcColHeight() {
  const gridCol = document.querySelectorAll('.schedule__grid-col')
  const lastChildArray = []
  for (let item = 0; item < gridCol.length; item++) {
    lastChildArray.push(parseInt(gridCol[item].lastElementChild.style.height))
    gridCol[item].style.height = `${getMaxOfArray(topArray) + getMaxOfArray(lastChildArray)}px`
  }
}

function assignEventListener() {
  for (let i = 0; i < scheduleMenuItem.length; i++) {
    scheduleMenuItem[i].addEventListener('click', handler)
  }
}

function toggleMenuInfo() {
  const scheduleTitle = document.querySelectorAll('.schedule__grid-block-title')
  const scheduleDesc = document.querySelectorAll('.schedule__grid-block-desc')
  const scheduleGridBlock = document.querySelectorAll('.schedule__grid-block')
  const menuTitle = document.querySelector('.schedule__menu-title')
  const menuDesc = document.querySelector('.schedule__menu-desc')
  if (window.pageYOffset >= scheduleGridBlock[1].getBoundingClientRect().top + pageYOffset) {
    menuTitle.innerHTML = scheduleTitle[1].innerText
    menuDesc.innerHTML = scheduleDesc[1].innerText
  } else {
    menuTitle.innerHTML = scheduleTitle[0].innerText
    menuDesc.innerHTML = scheduleDesc[0].innerText
  }
}


$(function() {
  $('.schedule__grid-mobile-select').change(function(){
    $('.schedule__grid-mobile-item').hide();
    $('#' + $(this).val()).show();
  });
});

setFullWidth()
calcEventHeight()
calcColHeight()
assignEventListener()
