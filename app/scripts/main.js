const hamburger = document.querySelector('.hamburger')
const mobileMenu = document.querySelector('.menu-mobile')

function openMenu() {
  hamburger.classList.toggle('is-active')
  mobileMenu.classList.toggle('is-active')
}

if (window.location.hash) scroll(0, 0)
setTimeout(function () {
  scroll(0, 0)
}, 1)

$(document).ready(setTimeout(function () {
  if (window.location.hash) {
    $('html, body').animate({
      scrollTop: $(window.location.hash).offset().top + 'px',
    }, 500, 'swing')
  }
}, 500))

hamburger.onclick = openMenu

// $(document).ready(function () {
//   $('a[href^="#"]').on('click', function (e) {
//     e.preventDefault()
//     const target = this.hash
//     const $target = $(target)
//
//     $('html, body').stop().animate({
//       'scrollTop': $target.offset().top,
//     }, 500, 'swing', function () {
//       window.location.hash = target
//     })
//   })
// })

$('a[href*="#"]')
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function (event) {
    if (
      location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '')
      &&
      location.hostname === this.hostname
    ) {
      var target = $(this.hash)
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']')
      if (target.length) {
        event.preventDefault()
        $('html, body').animate({
          scrollTop: target.offset().top,
        }, 500)
      }
    }
  })

$('.owl-carousel').each(function () {
  var owl = $(this)
  owl.owlCarousel({
    nav: true,
    responsiveClass: true,
    autoWidth: true,
    navClass: ['slider__nav slider__nav--prev', 'slider__nav slider__nav--next'],
    navText: ['', ''],
    responsive: {
      0: {
        items: 1,
        margin: 12,

      },
      600: {
        items: 3,
        margin: 15,
      },
      1024: {
        items: 4,
        margin: 15,
      },
      1280: {
        items: 4,
        margin: 20,
      },
    },
  })
})

window.fbAsyncInit = function () {
  FB.init({
    appId: '627580014250035',
    autoLogAppEvents: true,
    xfbml: true,
    version: 'v2.12',
  })
};

(function (d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0]
  if (d.getElementById(id)) {
    return
  }
  js = d.createElement(s)
  js.id = id
  js.src = 'https://connect.facebook.net/en_US/sdk.js'
  fjs.parentNode.insertBefore(js, fjs)
}(document, 'script', 'facebook-jssdk'));

(function ($) {
  $('#twShareBtn').click(function (e) {
    e.preventDefault()
    var href = $(this).attr('href')
    window.open(href, 'Twitter', 'width=600,height=400')
  })
  $('#vkShareBtn').click(function (e) {
    e.preventDefault()
    var href = `http://vk.com/share.php?url=${window.location.href}`
    window.open(href, 'Vkontakte', 'width=600,height=400')
  })
})(jQuery)

const fbShareBtn = document.getElementById('fbShareBtn')

if (fbShareBtn !== null) {
  fbShareBtn.onclick = function () {
    // FB.ui({
    //   method: 'share',
    //   display: 'popup',
    //   href: 'https://developers.facebook.com/docs/',
    // }, function (response) {
    // });
    window.open(`https://www.facebook.com/sharer.php?u=${window.location.href}`, 'Facebook', 'width=600,height=400')
  }
}

let delay = (() => {
  let timer = 0
  return function (callback, ms) {
    clearTimeout(timer)
    timer = setTimeout(callback, ms)
  }
})()

$('[data-search]').on('keyup', function () {
  const searchVal = $(this).val()
  const filterItems = $('[data-filter-item]')
  const lastItem = $('.slider__video-item--last')
  const parent = filterItems.parent()
  const filterResult = $('[data-filter-item][data-filter-name*="' + searchVal.toLowerCase() + '"]')
  const nav = $('.owl-nav')
  const stages = document.querySelectorAll('.owl-stage')
  delay(function () {
    if (searchVal !== '') {
      filterItems.addClass('hidden')
      lastItem.addClass('hidden')
      parent.addClass('hidden')
      filterResult.removeClass('hidden')
      filterResult.parent().removeClass('hidden')
      // nav.addClass('hidden');
      getStage(stages)
    } else {
      filterItems.removeClass('hidden')
      lastItem.removeClass('hidden')
      parent.removeClass('hidden')
      nav.removeClass('hidden')
    }
  }, 333)
})

function getItem(item) {
  let counter = 0
  const nav = $('.owl-nav')
  for (let cn = 0; cn <= item.childNodes.length - 1; cn++) {
    let activeITem = item.childNodes.length - counter
    item.childNodes[cn].classList.contains('hidden') ? counter++ : null
    activeITem < 4 ? nav.addClass('hidden') : nav.removeClass('hidden')
  }

}

function getStage(stage) {
  for (let i = 0; i <= stage.length - 1; i++) {
    getItem(stage[i])
  }
}

$(function () {
  objectFitImages()
})


// function modal() {
//   const speaker = document.querySelectorAll('.event__speakers-item')
//   const modal = document.querySelectorAll('.event__modal')
//   const modalClose = document.querySelectorAll('.event__modal-close')
//   const modalInner = document.querySelectorAll('.event__modal-inner')
//   for (let i = 0; i < speaker.length; i++) {
//     speaker[i].onclick = () => {
//       if (speaker[i].dataset.target === modal[i].dataset.modal) {
//         modal[i].style.display = 'block'
//         modalClose[i].onclick = () => modal[i].style.display = 'none'
//         window.onclick = function(event) {
//           if (event.target === modalInner[i]) {
//             modal[i].style.display = 'none';
//           }
//         }
//       } else {
//         null
//       }
//     }
//   }
// }
//
// modal()
MicroModal.init({
  disableScroll: true,
  disableFocus: true,
  awaitCloseAnimation: false,
  debugMode: true,
})
